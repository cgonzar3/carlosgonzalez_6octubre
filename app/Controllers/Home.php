<?php

namespace App\Controllers;

use App\Models\GrupoModel;
#use App\Models\AlumnoModel;
#use .....

class Home extends BaseController
{
    public function index() {
        return view('welcome_message');
    }

    public function muestraGrupo() {

        #$datosobtenidos['title'] = 'Listado de Alumnos';
        #Siempre se pone para enlazar con la base de datos
        $grupos = new GrupoModel();
        
        #Sentencia sql guardada en una variable
        $array ['title'] = "TablaGruposCarlos";
        ####esto es para el ej 4 #$array ['result'] = $datosobtenidos = $grupos->findAll();

        #ej 5:
        $textoabuscar = $this->request->getPost('valorbuscado');
        $array ['result'] = $grupos->like(['codigo' => strtoupper($textoabuscar)])->findAll();

        #imprimir variable con los datos
        /*
          echo "<pre>";
          print_r($datosobtenidos);
          echo "</pre>";
         */

        return view('grupocarlos/lista', $array);
    }

    public function buscaGrupo() {

        $array['title'] = "Carlos";

        return view('grupocarlos/formulario', $array);
    }

}
